package com.interview.schnotify.callback;

/**
 * Create by Sreylish on 12/13/2019
 */
public interface Callback {
    <E> void responseData(E response);
    void onFail(String msg);
}
