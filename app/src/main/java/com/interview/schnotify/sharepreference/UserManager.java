package com.interview.schnotify.sharepreference;

import android.content.SharedPreferences;

import com.interview.schnotify.Login.entity.Profile;

/**
 * Create by Sreylish on 12/13/2019
 */
public class UserManager {

    private static SharedPreferences pref;
    private static SharedPreferences.Editor editor;
    private static UserManager userManager = new UserManager();

    public static UserManager getInstance(SharedPreferences mPref){
        pref = mPref;
        editor = mPref.edit();
        return userManager;
    }
    public void saveUser(Profile profile){
        editor.putString("USER_ID",profile.getId());
        editor.putString("USER_NAME",profile.getName());
        editor.putString("PHONE",profile.getTel());
        editor.putString("PICTURE",profile.getPhoto());

        editor.apply();
    }
    public void savePassword(String pass){
        editor.putString("PASSWORD",pass);
        editor.apply();
    }
    public void saveToken(String token){
        editor.putString("TOKEN",token);
        editor.apply();
    }
    public String getToken(){
        return pref.getString("TOKEN","");
    }
    public String getUserName(){
        return pref.getString("USER_NAME","");
    }
    public String getUserPhone(){
        return pref.getString("PHONE","");
    }
    public String getPicture(){ return pref.getString("PICTURE",""); }
    public String getUserId(){ return pref.getString("USER_ID",""); }
    public String getPassword(){ return pref.getString("PASSWORD",""); }


}
