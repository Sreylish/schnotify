package com.interview.schnotify.retrofit;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Create by Sreylish on 12/13/2019
 */
public class AuthenticationInterceptor implements Interceptor {
    String authToken;
    public AuthenticationInterceptor(String authToken) {
        this.authToken = authToken;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
    Request original = chain.request();
    Request.Builder builder = original.newBuilder();
    if(!authToken.equals("")){
        builder.header("Authorization", authToken);
    }

    builder.addHeader("Accept", "application/json");
    Request request = builder.build();
    return chain.proceed(request);
    }
}
