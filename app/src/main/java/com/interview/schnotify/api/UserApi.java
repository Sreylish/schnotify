package com.interview.schnotify.api;

import android.media.Image;

import com.interview.schnotify.Login.entity.Profile;
import com.interview.schnotify.home.menu.entity.UpdateImageResponse;
import com.interview.schnotify.home.menu.entity.UpdateUserResponse;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Create by Sreylish on 12/14/2019
 */
public interface UserApi {

    @POST("api/update_mobile_user_name")
    Call<UpdateUserResponse> updateName(@Body Profile profile);

   @GET("api/user_photo")
    Call<ResponseBody> getImage(@Query("user_id") String user_id);

   @Multipart
   @POST("api/update/user-photo")
   Call<UpdateImageResponse> updateUserImage(@Part MultipartBody.Part file, @Query("Name") String Name, @Query("type") String type);
}
