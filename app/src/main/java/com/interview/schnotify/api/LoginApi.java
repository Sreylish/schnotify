package com.interview.schnotify.api;

import com.interview.schnotify.Login.entity.LoginResponse;
import com.interview.schnotify.Login.entity.Profile;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Create by Sreylish on 12/13/2019
 */
public interface LoginApi {
    @POST("api/login")
    Call<LoginResponse> login(@Body Profile profile);
}
