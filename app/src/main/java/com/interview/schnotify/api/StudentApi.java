package com.interview.schnotify.api;

import com.interview.schnotify.home.student.entity.StudentResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Create by Sreylish on 12/13/2019
 */
public interface StudentApi {
    @GET("api/load_student")
    Call<List<StudentResponse>> getStudent();
}
