package com.interview.schnotify.home.menu.entity;

import com.google.gson.annotations.SerializedName;

public class UpdateImageResponse {
    @SerializedName("code")
    private String code;

    @SerializedName("result")
    private String result;

    public void setCode(String code){
        this.code = code;
    }

    public String getCode(){
        return code;
    }

    public void setResult(String result){
        this.result = result;
    }

    public String getResult(){
        return result;
    }

}
