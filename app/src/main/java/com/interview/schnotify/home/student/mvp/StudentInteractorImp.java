package com.interview.schnotify.home.student.mvp;

import android.content.Context;
import android.util.Log;

import com.interview.schnotify.api.StudentApi;
import com.interview.schnotify.callback.Callback;
import com.interview.schnotify.home.student.entity.StudentResponse;
import com.interview.schnotify.retrofit.ServiceGenerator;
import com.interview.schnotify.sharepreference.UserManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Create by Sreylish on 12/13/2019
 */
public class StudentInteractorImp implements StudentContract.StudentInteracor {
    private Context context;

    public StudentInteractorImp(Context context) {
        this.context = context;
    }

    @Override
    public void getStudent(final Callback callback) {
        UserManager userManager = UserManager.getInstance(context.getSharedPreferences("USER",Context.MODE_PRIVATE));
        StudentApi api = ServiceGenerator.createService(StudentApi.class,"Bearer "+userManager.getToken());
        api.getStudent().enqueue(new retrofit2.Callback<List<StudentResponse>>() {
            @Override
            public void onResponse(Call<List<StudentResponse>> call, Response<List<StudentResponse>> response) {
                if(response.isSuccessful()){
                    callback.responseData(response.body());
                }else{
                    callback.onFail(response.message());
                }
            }

            @Override
            public void onFailure(Call<List<StudentResponse>> call, Throwable t) {
                callback.onFail("Can't connect to server");
            }
        });
    }
}
