package com.interview.schnotify.home.student.mvp;

import com.interview.schnotify.Login.entity.Profile;
import com.interview.schnotify.callback.Callback;

/**
 * Create by Sreylish on 12/13/2019
 */
public interface StudentContract {
    interface StudentView{
        void onShowLoading();
        void onHideLoading();
        <E> void onResponse(E res);
        void onError(String msg);
    }

    interface StudentPresenter{
        void getStudent();
    }
    interface StudentInteracor{
        void getStudent(Callback callback);
    }
}
