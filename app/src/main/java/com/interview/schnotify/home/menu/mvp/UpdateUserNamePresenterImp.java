package com.interview.schnotify.home.menu.mvp;

import android.content.Context;

import com.interview.schnotify.Login.entity.Profile;
import com.interview.schnotify.callback.Callback;

import okhttp3.MultipartBody;

/**
 * Create by Sreylish on 12/14/2019
 */
public class UpdateUserNamePresenterImp implements UpdateUserName.UpdateUserNamePresenter {
    private UpdateUserName.UpdateUserNameView view;
    private Context context;
    private UpdateUserName.UpdateUserNameInteracor interacor;

    public UpdateUserNamePresenterImp(UpdateUserName.UpdateUserNameView view,Context context) {
        this.view = view;
        this.context = context;
        interacor = new UpdateUserNameInteractorImp(context);
    }

    @Override
    public void updateUserName(Profile profile) {
        view.onShowLoading();
        interacor.updateUserName(profile, new Callback() {
            @Override
            public <E> void responseData(E response) {
                view.onResponse(response);
                view.onHideLoading();
            }

            @Override
            public void onFail(String msg) {
                view.onError(msg);
                view.onHideLoading();
            }
        });
    }

    @Override
    public void login(Profile profile) {
        interacor.login(profile, new Callback() {
            @Override
            public <E> void responseData(E response) {
                view.onLoginResponse(response);
            }

            @Override
            public void onFail(String msg) {
                view.onError(msg);
            }
        });
    }

    @Override
    public void getImage(String user_id) {
        interacor.getImage(user_id, new Callback() {
            @Override
            public <E> void responseData(E response) {
                view.onGetUserImage(response);
            }

            @Override
            public void onFail(String msg) {
                view.onError(msg);
            }
        });
    }

    @Override
    public void updateUserImage(MultipartBody.Part file, String Name, String type) {
        view.onShowLoading();
        interacor.updateUserImage(file, Name, type, new Callback() {
            @Override
            public <E> void responseData(E response) {
                view.onUpdateImageResponse(response);
                view.onHideLoading();
            }

            @Override
            public void onFail(String msg) {
                view.onError(msg);
                view.onHideLoading();
            }
        });
    }
}
