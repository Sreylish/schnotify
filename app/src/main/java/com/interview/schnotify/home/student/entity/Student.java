package com.interview.schnotify.home.student.entity;

import java.util.List;

/**
 * Create by Sreylish on 12/13/2019
 */
public class Student {
    private List<StudentResponse> list;

    public List<StudentResponse> getList() {
        return list;
    }

    public void setList(List<StudentResponse> list) {
        this.list = list;
    }

}
