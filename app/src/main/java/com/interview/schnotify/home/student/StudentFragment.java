package com.interview.schnotify.home.student;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.interview.schnotify.R;
import com.interview.schnotify.home.student.adapter.StudentAdapter;
import com.interview.schnotify.home.student.entity.StudentResponse;
import com.interview.schnotify.home.student.mvp.StudentContract;
import com.interview.schnotify.home.student.mvp.StudentPresenterImp;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;
import java.util.List;

public class StudentFragment extends Fragment implements StudentContract.StudentView {

    public StudentFragment() {
        // Required empty public constructor
    }

    private StudentAdapter adapter;
    private StudentContract.StudentPresenter presenter;
    private List<StudentResponse> list = new ArrayList<>();
    private KProgressHUD kProgressHUD;
    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_prfile, container, false);
        presenter = new StudentPresenterImp(this,getContext());
        Tovuti.from(getContext()).monitor(new Monitor.ConnectivityListener(){
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast){
                if(isConnected){
                   presenter.getStudent();
                }else{
                    dialogNoInternet();
                }
            }
        });
        adapter = new StudentAdapter(list);
        RecyclerView recyclerView = view.findViewById(R.id.rv_student);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        return view;
    }

    private void dialogNoInternet() {
        final AlertDialog.Builder builder  = new AlertDialog.Builder(getContext());
        builder.setMessage(getResources().getString(R.string.no_internet));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    @Override
    public void onShowLoading() {
        kProgressHUD = KProgressHUD.create(requireContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.6f)
                .show();

    }

    @Override
    public void onHideLoading() {
        kProgressHUD.dismiss();
    }

    @Override
    public <E> void onResponse(E res) {
        List<StudentResponse> responses = (List<StudentResponse>) res;
        list.addAll(responses);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onError(String msg) {
        Toast.makeText(requireContext(),msg, Toast.LENGTH_SHORT).show();

    }
}
