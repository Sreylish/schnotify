package com.interview.schnotify.home.menu.mvp;

import com.interview.schnotify.Login.entity.Profile;
import com.interview.schnotify.callback.Callback;

import okhttp3.MultipartBody;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Create by Sreylish on 12/14/2019
 */
public interface UpdateUserName {
    interface UpdateUserNameView{
        void onShowLoading();
        void onHideLoading();
        <E> void onResponse(E res);
        <E> void onLoginResponse (E res);
        <E> void onGetUserImage(E res);
        <E> void onUpdateImageResponse(E res);
        void onError(String msg);

    }

    interface UpdateUserNamePresenter{
        void updateUserName(Profile profile);
        void login(Profile profile);
        void getImage(String user_id);
        void updateUserImage(@Part MultipartBody.Part file, String Name, String type);
    }
    interface UpdateUserNameInteracor{
        void login(Profile profile,Callback callback);
        void updateUserName(Profile profile, Callback callback);
        void getImage(String user_id, Callback callback);
        void updateUserImage(@Part MultipartBody.Part file, String Name, String type,Callback callback);
    }
}
