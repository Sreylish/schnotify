package com.interview.schnotify.home.student.mvp;

import android.content.Context;

import com.interview.schnotify.callback.Callback;

/**
 * Create by Sreylish on 12/13/2019
 */
public class StudentPresenterImp implements StudentContract.StudentPresenter {
    private StudentContract.StudentView view;
    private StudentContract.StudentInteracor interacor;
    private Context context;
    public StudentPresenterImp(StudentContract.StudentView view,Context context) {
        this.view = view;
        this.context = context;
        interacor = new StudentInteractorImp(context);
    }

    @Override
    public void getStudent() {
        view.onShowLoading();
        interacor.getStudent(new Callback() {
            @Override
            public <E> void responseData(E response) {
                view.onResponse(response);
                view.onHideLoading();
            }

            @Override
            public void onFail(String msg) {
                view.onError(msg);
                view.onHideLoading();
            }
        });
    }
}
