package com.interview.schnotify.home.student.entity;

import java.util.Collection;

public class StudentResponse {
	private String endDate;
	private String teacherName;
	private String levelId;
	private String idCard;
	private Object representativeName;
	private Object oldStartTime;
	private String teacherAssistant;
	private Object representativeNumber;
	private String balance;
	private String schoolId;
	private String branchId;
	private String branchName;
	private String fingerprint;
	private String id;
	private String idNumber;
	private Object odpStatus;
	private String level;
	private String sex;
	private String endTime;
	private String schoolName;
	private String photo;
	private Object representativeRelation;
	private String room;
	private String startTime;
	private String nameKh;
	private Object oldTeacherAssistant;
	private String dob;
	private String nickName;
	private Object oldEndTime;
	private Object odp;
	private Object oldTeacherName;
	private String nameEn;
	private String status;

	public void setEndDate(String endDate){
		this.endDate = endDate;
	}

	public String getEndDate(){
		return endDate;
	}

	public void setTeacherName(String teacherName){
		this.teacherName = teacherName;
	}

	public String getTeacherName(){
		return teacherName;
	}

	public void setLevelId(String levelId){
		this.levelId = levelId;
	}

	public String getLevelId(){
		return levelId;
	}

	public void setIdCard(String idCard){
		this.idCard = idCard;
	}

	public String getIdCard(){
		return idCard;
	}

	public void setRepresentativeName(Object representativeName){
		this.representativeName = representativeName;
	}

	public Object getRepresentativeName(){
		return representativeName;
	}

	public void setOldStartTime(Object oldStartTime){
		this.oldStartTime = oldStartTime;
	}

	public Object getOldStartTime(){
		return oldStartTime;
	}

	public void setTeacherAssistant(String teacherAssistant){
		this.teacherAssistant = teacherAssistant;
	}

	public String getTeacherAssistant(){
		return teacherAssistant;
	}

	public void setRepresentativeNumber(Object representativeNumber){
		this.representativeNumber = representativeNumber;
	}

	public Object getRepresentativeNumber(){
		return representativeNumber;
	}

	public void setBalance(String balance){
		this.balance = balance;
	}

	public String getBalance(){
		return balance;
	}

	public void setSchoolId(String schoolId){
		this.schoolId = schoolId;
	}

	public String getSchoolId(){
		return schoolId;
	}

	public void setBranchId(String branchId){
		this.branchId = branchId;
	}

	public String getBranchId(){
		return branchId;
	}

	public void setBranchName(String branchName){
		this.branchName = branchName;
	}

	public String getBranchName(){
		return branchName;
	}

	public void setFingerprint(String fingerprint){
		this.fingerprint = fingerprint;
	}

	public String getFingerprint(){
		return fingerprint;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setIdNumber(String idNumber){
		this.idNumber = idNumber;
	}

	public String getIdNumber(){
		return idNumber;
	}

	public void setOdpStatus(Object odpStatus){
		this.odpStatus = odpStatus;
	}

	public Object getOdpStatus(){
		return odpStatus;
	}

	public void setLevel(String level){
		this.level = level;
	}

	public String getLevel(){
		return level;
	}

	public void setSex(String sex){
		this.sex = sex;
	}

	public String getSex(){
		return sex;
	}

	public void setEndTime(String endTime){
		this.endTime = endTime;
	}

	public String getEndTime(){
		return endTime;
	}

	public void setSchoolName(String schoolName){
		this.schoolName = schoolName;
	}

	public String getSchoolName(){
		return schoolName;
	}

	public void setPhoto(String photo){
		this.photo = photo;
	}

	public String getPhoto(){
		return photo;
	}

	public void setRepresentativeRelation(Object representativeRelation){
		this.representativeRelation = representativeRelation;
	}

	public Object getRepresentativeRelation(){
		return representativeRelation;
	}

	public void setRoom(String room){
		this.room = room;
	}

	public String getRoom(){
		return room;
	}

	public void setStartTime(String startTime){
		this.startTime = startTime;
	}

	public String getStartTime(){
		return startTime;
	}

	public void setNameKh(String nameKh){
		this.nameKh = nameKh;
	}

	public String getNameKh(){
		return nameKh;
	}

	public void setOldTeacherAssistant(Object oldTeacherAssistant){
		this.oldTeacherAssistant = oldTeacherAssistant;
	}

	public Object getOldTeacherAssistant(){
		return oldTeacherAssistant;
	}

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setNickName(String nickName){
		this.nickName = nickName;
	}

	public String getNickName(){
		return nickName;
	}

	public void setOldEndTime(Object oldEndTime){
		this.oldEndTime = oldEndTime;
	}

	public Object getOldEndTime(){
		return oldEndTime;
	}

	public void setOdp(Object odp){
		this.odp = odp;
	}

	public Object getOdp(){
		return odp;
	}

	public void setOldTeacherName(Object oldTeacherName){
		this.oldTeacherName = oldTeacherName;
	}

	public Object getOldTeacherName(){
		return oldTeacherName;
	}

	public void setNameEn(String nameEn){
		this.nameEn = nameEn;
	}

	public String getNameEn(){
		return nameEn;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"StudentResponse{" + 
			"end_date = '" + endDate + '\'' + 
			",teacher_name = '" + teacherName + '\'' + 
			",level_id = '" + levelId + '\'' + 
			",id_card = '" + idCard + '\'' + 
			",representative_name = '" + representativeName + '\'' + 
			",old_start_time = '" + oldStartTime + '\'' + 
			",teacher_assistant = '" + teacherAssistant + '\'' + 
			",representative_number = '" + representativeNumber + '\'' + 
			",balance = '" + balance + '\'' + 
			",school_id = '" + schoolId + '\'' + 
			",branch_id = '" + branchId + '\'' + 
			",branch_name = '" + branchName + '\'' + 
			",fingerprint = '" + fingerprint + '\'' + 
			",id = '" + id + '\'' + 
			",id_number = '" + idNumber + '\'' + 
			",odp_status = '" + odpStatus + '\'' + 
			",level = '" + level + '\'' + 
			",sex = '" + sex + '\'' + 
			",end_time = '" + endTime + '\'' + 
			",school_name = '" + schoolName + '\'' + 
			",photo = '" + photo + '\'' + 
			",representative_relation = '" + representativeRelation + '\'' + 
			",room = '" + room + '\'' + 
			",start_time = '" + startTime + '\'' + 
			",name_kh = '" + nameKh + '\'' + 
			",old_teacher_assistant = '" + oldTeacherAssistant + '\'' + 
			",dob = '" + dob + '\'' + 
			",nick_name = '" + nickName + '\'' + 
			",old_end_time = '" + oldEndTime + '\'' + 
			",odp = '" + odp + '\'' + 
			",old_teacher_name = '" + oldTeacherName + '\'' + 
			",name_en = '" + nameEn + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
