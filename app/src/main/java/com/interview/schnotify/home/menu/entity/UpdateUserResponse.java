package com.interview.schnotify.home.menu.entity;

import com.google.gson.annotations.SerializedName;

public class UpdateUserResponse{

	@SerializedName("code")
	private String code;

	@SerializedName("status")
	private String status;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

}