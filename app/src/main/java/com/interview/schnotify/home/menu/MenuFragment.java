package com.interview.schnotify.home.menu;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;


import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.interview.schnotify.BuildConfig;
import com.interview.schnotify.Login.entity.LoginResponse;
import com.interview.schnotify.Login.entity.Profile;
import com.interview.schnotify.R;
import com.interview.schnotify.home.menu.mvp.UpdateUserName;
import com.interview.schnotify.home.menu.mvp.UpdateUserNamePresenterImp;
import com.interview.schnotify.sharepreference.UserManager;
import com.kaopiz.kprogresshud.KProgressHUD;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment implements UpdateUserName.UpdateUserNameView {


    private static final int PERMISSIONS_MULTIPLE_REQUEST = 1;
    private static final int REQUEST_CAPTURE_IMAGE = 2;
    private static final int GALLERY_REQUEST_CODE = 3;
    private UpdateUserName.UpdateUserNamePresenter presenter;
    private Profile profile;
    private UserManager userManager;
    private String name;
    private TextView txtName;
    private ImageView imageView;
    private String imageFilePath;
    private File fileImage;
    private boolean uploadFromGallery;
    private boolean uploadFromCamera;
    private boolean updateUserName;
    private Uri uri;
    private MultipartBody.Part part;
    private KProgressHUD kProgressHUD;

    public MenuFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        txtName = view.findViewById(R.id.txt_name);
        TextView txtPhone = view.findViewById(R.id.txt_phone);
        ImageView btnEditPhoto = view.findViewById(R.id.btn_edit_photo);
        imageView = view.findViewById(R.id.circleImageView);

        ImageView editName = view.findViewById(R.id.edt_name);
        presenter = new UpdateUserNamePresenterImp(this, getContext());
        userManager = UserManager.getInstance(Objects.requireNonNull(getContext()).getSharedPreferences("USER", Context.MODE_PRIVATE));
        Tovuti.from(getContext()).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                if (isConnected) {
                    presenter.getImage(userManager.getUserId());
                } else {
                    dialogNoInternet();
                }
            }
        });
        if (userManager.getUserName().equals("") || userManager.getUserName() == null)
            txtName.setText("N/A");
        else
            txtName.setText(userManager.getUserName());
        if (userManager.getUserPhone().equals("") || userManager.getUserPhone() == null)
            txtPhone.setText("N/A");
        else
            txtPhone.setText(userManager.getUserPhone());
        editName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
        btnEditPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermission();
            }
        });
        return view;
    }

    private void dialogNoInternet() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(getResources().getString(R.string.no_internet));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    private void checkPermission() {
        if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.checkSelfPermission(requireActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_MULTIPLE_REQUEST);
            }
        } else {
            dialogUpdatePhoto();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_MULTIPLE_REQUEST) {
            if (grantResults.length > 0) {
                boolean cameraPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (cameraPermission && readExternalFile) {
                    dialogUpdatePhoto();
                }
            }
        }
    }

    private void dialogUpdatePhoto() {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(requireActivity());
        bottomSheetDialog.setContentView(R.layout.cu_update_photo_dialog);
        TextView camera = bottomSheetDialog.findViewById(R.id.txt_camera);
        assert camera != null;
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
                bottomSheetDialog.dismiss();
            }
        });
        TextView photo = bottomSheetDialog.findViewById(R.id.txt_gallery);
        if (photo != null) {
            photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Objects.requireNonNull(getActivity()).getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    String[] mimeTypes = {"image/jpeg", "image/png"};
                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                    startActivityForResult(intent, GALLERY_REQUEST_CODE);
                    bottomSheetDialog.dismiss();
                }
            });
        }
        bottomSheetDialog.show();
    }

    private String getRealPathFromURI(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        @SuppressWarnings("deprecation")
        Cursor cursor = requireActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void openCamera() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(requireActivity().getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(requireActivity(), BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(pictureIntent, REQUEST_CAPTURE_IMAGE);
            }
        }
    }

    private File createImageFile() throws IOException {
        String imageFileName = "IMG_" + "_";
        File storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        fileImage = File.createTempFile(imageFileName, ".jpg", storageDir);
        imageFilePath = fileImage.getAbsolutePath();
        Log.e("kkkk", imageFilePath);

        return fileImage;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Objects.requireNonNull(getActivity()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        if (resultCode == RESULT_OK) {
            RequestBody requestBody;
            if (requestCode == REQUEST_CAPTURE_IMAGE) {
                uploadFromCamera = true;
                uploadFromGallery = false;
                updateUserName = false;
                fileImage = new File(fileImage.getPath());
                requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), fileImage);
                part = MultipartBody.Part.createFormData("file", fileImage.getName(), requestBody);
                presenter.updateUserImage(part, endCode("Value"), endCode("android"));
            } else if (requestCode == GALLERY_REQUEST_CODE) {
                if (data != null && data.getData() != null) {
                    uri = data.getData();
                    uploadFromGallery = true;
                    uploadFromCamera = false;
                    updateUserName = false;
                    File file = new File(getRealPathFromURI(data.getData()));
                    requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    part = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
                    presenter.updateUserImage(part, endCode("Value"), endCode("android"));
                }

            }
        }
    }

    private boolean downloadImage(ResponseBody body) {
        try {
            File futureStudioIconFile = new File(requireActivity().getExternalFilesDir(null) + File.separator + userManager.getPicture());

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[1024];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.d("download", "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();
                Bitmap bMap = BitmapFactory.decodeFile(requireActivity().getExternalFilesDir(null) + File.separator + userManager.getPicture());
                int width = 4 * bMap.getWidth();
                int height = 4 * bMap.getHeight();
                Bitmap bMap2 = Bitmap.createScaledBitmap(bMap, width, height, false);
                imageView.setImageBitmap(bMap2);

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }

        } catch (IOException e) {
            return false;
        }
    }


    private void showDialog() {
        final Dialog dialog = new Dialog(requireContext());
        @SuppressLint("InflateParams")
        View view = getLayoutInflater().inflate(R.layout.cu_dialog_update_name, null);
        dialog.setContentView(view);
        Objects.requireNonNull(dialog.getWindow()).setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final EditText edtName = view.findViewById(R.id.ed_name);
        edtName.setText(txtName.getText().toString());
        /*   edtName.setText("user");*/
        TextView cancel = view.findViewById(R.id.txt_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        TextView update = view.findViewById(R.id.txt_update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = edtName.getText().toString();
                if (!name.equals("")) {
                    updateUserName = true;
                    requestToken();
                } else {
                    edtName.setError("Please fill your name");
                }
                dialog.dismiss();
            }
        });
        dialog.create();
        dialog.show();
    }

    private void requestToken() {
        profile = new Profile();
        profile.setName(endCode("value"));
        profile.setCode(endCode("123456"));
        profile.setTel(endCode("069342618"));
        profile.setLogin_type(endCode("parent"));
        profile.setDevice_key(endCode("0"));
        profile.setType(endCode("android"));
        profile.setApp_id(endCode("trendsecsolution.com.schoolnotificationappdev"));
        presenter.login(profile);
    }

    private String endCode(String text) {
        byte[] data = new byte[0];
        data = text.getBytes(StandardCharsets.UTF_8);
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    @Override
    public void onShowLoading() {
        kProgressHUD = KProgressHUD.create(requireActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

    }

    @Override
    public void onHideLoading() {
        kProgressHUD.dismiss();
    }

    @Override
    public <E> void onResponse(E res) {
        txtName.setText(name);
    }

    @Override
    public <E> void onLoginResponse(E res) {
        LoginResponse response = (LoginResponse) res;
        UserManager userManager = UserManager.getInstance(requireActivity().getSharedPreferences("USER", Context.MODE_PRIVATE));
        userManager.saveToken(response.getToken());
        profile = new Profile();
        profile.setName(name);
        if (updateUserName) {
            presenter.updateUserName(profile);
        } else {
            presenter.updateUserImage(part, endCode("Value"), endCode("android"));
        }
    }

    @Override
    public <E> void onGetUserImage(E res) {
        ResponseBody responseBody = (ResponseBody) res;
        downloadImage(responseBody);

    }

    @Override
    public <E> void onUpdateImageResponse(E res) {
        if (uploadFromGallery) {
            imageView.setImageURI(uri);
        }
        if (uploadFromCamera) {
            imageView.setImageDrawable(Drawable.createFromPath(imageFilePath));
        }
    }

    @Override
    public void onError(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
