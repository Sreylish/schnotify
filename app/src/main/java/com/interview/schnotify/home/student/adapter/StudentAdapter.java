package com.interview.schnotify.home.student.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.interview.schnotify.R;
import com.interview.schnotify.home.student.entity.StudentResponse;

import java.util.List;

/**
 * Create by Sreylish on 12/13/2019
 */
public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder>{
    private List<StudentResponse> list;

    public StudentAdapter(List<StudentResponse> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cu_student_adapter,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        if(list.get(i).getNameEn() == null || list.get(i).getNameEn().equals("")) {
            holder.stuName.setText("N/A");
        } else{
                holder.stuName.setText(list.get(i).getNameEn());
        }
        if(list.get(i).getSchoolName() == null || list.get(i).getSchoolName().equals("")) {
            holder.schoolName.setText("N/A");
        } else{
            holder.schoolName.setText(list.get(i).getSchoolName());
        }
        if(list.get(i).getSex() == null || list.get(i).getSex().equals("")) {
            holder.gender.setText("N/A");
        } else{
            holder.gender.setText(list.get(i).getSex());
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageView;
        private TextView stuName, schoolName,gender;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            stuName = itemView.findViewById(R.id.txt_name);
            schoolName = itemView.findViewById(R.id.txt_school);
            gender  = itemView.findViewById(R.id.txt_gender);
        }
    }
}
