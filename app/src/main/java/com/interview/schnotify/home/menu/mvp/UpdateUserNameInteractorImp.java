package com.interview.schnotify.home.menu.mvp;

import android.content.Context;

import com.interview.schnotify.Login.entity.LoginResponse;
import com.interview.schnotify.Login.entity.Profile;
import com.interview.schnotify.api.LoginApi;
import com.interview.schnotify.api.UserApi;
import com.interview.schnotify.callback.Callback;
import com.interview.schnotify.home.menu.entity.UpdateImageResponse;
import com.interview.schnotify.home.menu.entity.UpdateUserResponse;
import com.interview.schnotify.retrofit.ServiceGenerator;
import com.interview.schnotify.sharepreference.UserManager;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Create by Sreylish on 12/14/2019
 */
public class UpdateUserNameInteractorImp implements UpdateUserName.UpdateUserNameInteracor {

    private Context context;
    public UpdateUserNameInteractorImp(Context context) {
        this.context = context;
    }

    @Override
    public void login(Profile profile, final Callback callback) {
        LoginApi api = ServiceGenerator.createService(LoginApi.class,"");
        api.login(profile).enqueue(new retrofit2.Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()){
                    callback.responseData(response.body());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                callback.onFail("Can't connect to server");
            }
        });
    }

    @Override
    public void updateUserName(Profile profile, final Callback callback) {
        UserManager userManager = UserManager.getInstance(context.getSharedPreferences("USER",Context.MODE_PRIVATE));
        UserApi api = ServiceGenerator.createService(UserApi.class,"Bearer "+userManager.getToken());
        api.updateName(profile).enqueue(new retrofit2.Callback<UpdateUserResponse>() {
            @Override
            public void onResponse(Call<UpdateUserResponse> call, Response<UpdateUserResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getStatus().equals("success")){
                        callback.responseData(response.body());
                    }else{
                        callback.onFail(response.body().getStatus());
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateUserResponse> call, Throwable t) {
                callback.onFail("Can't connect to server");
            }
        });
    }

    @Override
    public void getImage(String user_id, final Callback callback) {
        UserManager userManager = UserManager.getInstance(context.getSharedPreferences("USER",Context.MODE_PRIVATE));
        UserApi api = ServiceGenerator.createService(UserApi.class,"Bearer "+userManager.getToken());
        api.getImage(user_id).enqueue(new retrofit2.Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    callback.responseData(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onFail("Can't connect to server");
            }
        });
    }

    @Override
    public void updateUserImage(MultipartBody.Part file, String Name, String type, final Callback callback) {
        UserManager userManager = UserManager.getInstance(context.getSharedPreferences("USER",Context.MODE_PRIVATE));
        UserApi api = ServiceGenerator.createService(UserApi.class,"Bearer "+userManager.getToken());
        api.updateUserImage(file,Name,type).enqueue(new retrofit2.Callback<UpdateImageResponse>() {
            @Override
            public void onResponse(Call<UpdateImageResponse> call, Response<UpdateImageResponse> response) {
                if(response.isSuccessful()) {
                    if (response.body().getResult().equals("success")) {
                        callback.responseData(response.body());
                    }else{
                        callback.onFail(response.body().getResult());
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateImageResponse> call, Throwable t) {
                callback.onFail("Can't connect to server");
            }
        });
    }
}
