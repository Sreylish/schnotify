package com.interview.schnotify.Login.mvp;

import com.interview.schnotify.Login.entity.Profile;
import com.interview.schnotify.callback.Callback;

/**
 * Create by Sreylish on 12/13/2019
 */
public interface LoginContract {
    interface LoginView{
        void onShowLoading();
        void onHideLoading();
        <E> void onResponse(E res);
        void onError(String msg);
    }

    interface LoginPresenter{
        void login(Profile profile);
    }
    interface LoginInteracor{
        void login(Profile profile, Callback callback);
    }
}
