package com.interview.schnotify.Login.mvp;

import com.interview.schnotify.Login.entity.Profile;
import com.interview.schnotify.callback.Callback;

/**
 * Create by Sreylish on 12/13/2019
 */
public class LoginPresenterImp implements LoginContract.LoginPresenter {
    private LoginContract.LoginView view;
    private LoginContract.LoginInteracor interacor;

    public LoginPresenterImp(LoginContract.LoginView loginView) {
        this.view = loginView;
        interacor = new LoginInteractorImp();
    }

    @Override
    public void login(Profile profile) {
        view.onShowLoading();
        interacor.login(profile, new Callback() {
            @Override
            public <E> void responseData(E response) {
                view.onResponse(response);
                view.onHideLoading();
            }

            @Override
            public void onFail(String msg) {
                view.onHideLoading();
                view.onError(msg);
            }
        });
    }
}
