package com.interview.schnotify.Login.entity;

import com.google.gson.annotations.SerializedName;


public class Features{

	@SerializedName("staff_chat")
	private int staffChat;

	@SerializedName("chat")
	private int chat;

	@SerializedName("timeline")
	private int timeline;

	@SerializedName("master_acc")
	private int masterAcc;

	public void setStaffChat(int staffChat){
		this.staffChat = staffChat;
	}

	public int getStaffChat(){
		return staffChat;
	}

	public void setChat(int chat){
		this.chat = chat;
	}

	public int getChat(){
		return chat;
	}

	public void setTimeline(int timeline){
		this.timeline = timeline;
	}

	public int getTimeline(){
		return timeline;
	}

	public void setMasterAcc(int masterAcc){
		this.masterAcc = masterAcc;
	}

	public int getMasterAcc(){
		return masterAcc;
	}

}