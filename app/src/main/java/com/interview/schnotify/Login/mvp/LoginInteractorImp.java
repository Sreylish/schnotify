package com.interview.schnotify.Login.mvp;

import com.interview.schnotify.Login.entity.LoginResponse;
import com.interview.schnotify.Login.entity.Profile;
import com.interview.schnotify.api.LoginApi;
import com.interview.schnotify.callback.Callback;
import com.interview.schnotify.retrofit.ServiceGenerator;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Create by Sreylish on 12/13/2019
 */
public class LoginInteractorImp implements LoginContract.LoginInteracor {
    @Override
    public void login(Profile profile, final Callback callback) {
        LoginApi api = ServiceGenerator.createService(LoginApi.class,"");
        api.login(profile).enqueue(new retrofit2.Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()){
                        callback.responseData(response.body());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                callback.onFail("Can't connect to server");
            }
        });

    }
}
