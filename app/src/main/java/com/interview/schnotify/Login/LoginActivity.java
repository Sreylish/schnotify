package com.interview.schnotify.Login;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidstudy.networkmanager.Monitor;
import com.androidstudy.networkmanager.Tovuti;
import com.interview.schnotify.Login.entity.LoginResponse;
import com.interview.schnotify.Login.entity.Profile;
import com.interview.schnotify.Login.mvp.LoginContract;
import com.interview.schnotify.Login.mvp.LoginPresenterImp;
import com.interview.schnotify.R;
import com.interview.schnotify.home.MainActivity;
import com.interview.schnotify.sharepreference.UserManager;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.UnsupportedEncodingException;

public class LoginActivity extends AppCompatActivity implements LoginContract.LoginView {

    private EditText edtPhone;
    private EditText edtPassword;
    private TextView btnSignIn;
    private LoginContract.LoginPresenter presenter;
    private Profile profile = new Profile();
    private KProgressHUD kProgressHUD;
    private UserManager userManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtPhone = findViewById(R.id.edt_phone);
        edtPassword = findViewById(R.id.edt_password);
        btnSignIn = findViewById(R.id.btn_sing_in);
        presenter = new LoginPresenterImp(this);
        userManager = UserManager.getInstance(getSharedPreferences("USER", MODE_PRIVATE));
        Log.e("kkkk",userManager.getUserName());
        Tovuti.from(this).monitor(new Monitor.ConnectivityListener() {
            @Override
            public void onConnectivityChanged(int connectionType, boolean isConnected, boolean isFast) {
                if (isConnected) {
                    if(!userManager.getUserId().equals("")){
                        edtPhone.setText(userManager.getUserPhone());
                        edtPassword.setText(userManager.getPassword());
                        login();
                    }
                    btnSignIn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            TextView fillPhone = findViewById(R.id.fill_phone);
                            TextView fillPassowrd = findViewById(R.id.fill_password);
                            if(edtPhone.getText().toString().equals("") && edtPassword.getText().toString().equals("")){
                                fillPhone.setVisibility(View.VISIBLE);
                                fillPassowrd.setVisibility(View.VISIBLE);
                            }else if(edtPassword.getText().toString().equals("")){
                                fillPassowrd.setVisibility(View.VISIBLE);
                                fillPhone.setVisibility(View.GONE);
                            }else if(edtPhone.getText().toString().equals("") ){
                                fillPhone.setVisibility(View.VISIBLE);
                                fillPassowrd.setVisibility(View.GONE);
                            }else if(endCode(edtPhone.getText().toString()).equals(endCode("069342618")) && endCode(edtPassword.getText().toString()).equals(endCode("123456"))){
                                fillPhone.setVisibility(View.GONE);
                                fillPassowrd.setVisibility(View.GONE);
                                login();
                            }else{
                                Toast.makeText(LoginActivity.this, "Invalid phone or password", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });
                } else {
                    dialogNoInternet();
                }
            }
        });
    }

    private void dialogNoInternet() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setMessage(getResources().getString(R.string.no_internet));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create();
        builder.show();
    }

    private void login() {
        profile.setName(endCode(edtPhone.getText().toString()));
        profile.setCode(endCode(edtPassword.getText().toString()));
        profile.setTel(endCode("069342618"));
        profile.setLogin_type(endCode("parent"));
        profile.setDevice_key(endCode("0"));
        profile.setType(endCode("android"));
        profile.setApp_id(endCode("trendsecsolution.com.schoolnotificationappdev"));
        presenter.login(profile);

    }

    public String endCode(String text) {
        byte[] data = new byte[0];
        try {
            data = text.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    @Override
    public void onShowLoading() {
        kProgressHUD = KProgressHUD.create(LoginActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    @Override
    public void onHideLoading() {
        kProgressHUD.dismiss();
    }

    @Override
    public <E> void onResponse(E res) {
        LoginResponse response = (LoginResponse) res;
        userManager.saveToken(response.getToken());
        userManager.savePassword(edtPassword.getText().toString());
        Profile profile = new Profile();
        profile.setName(response.getProfile().getName());
        profile.setTel(edtPhone.getText().toString());
        profile.setPhoto(response.getProfile().getPhoto());
        profile.setId(response.getProfile().getId());
        userManager.saveUser(profile);
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    public void onError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        Tovuti.from(this).stop();
        super.onDestroy();
    }
}
