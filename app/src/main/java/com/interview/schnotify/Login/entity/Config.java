package com.interview.schnotify.Login.entity;

import com.google.gson.annotations.SerializedName;


public class Config{

	@SerializedName("bus_interval")
	private String busInterval;

	public void setBusInterval(String busInterval){
		this.busInterval = busInterval;
	}

	public String getBusInterval(){
		return busInterval;
	}

}