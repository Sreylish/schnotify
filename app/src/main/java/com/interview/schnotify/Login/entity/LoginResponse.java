package com.interview.schnotify.Login.entity;

import com.google.gson.annotations.SerializedName;


public class LoginResponse{

	@SerializedName("result")
	private String result;

	@SerializedName("features")
	private Features features;

	@SerializedName("code")
	private String code;

	@SerializedName("profile")
	private Profile profile;

	@SerializedName("config")
	private Config config;

	@SerializedName("token")
	private String token;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setFeatures(Features features){
		this.features = features;
	}

	public Features getFeatures(){
		return features;
	}

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setProfile(Profile profile){
		this.profile = profile;
	}

	public Profile getProfile(){
		return profile;
	}

	public void setConfig(Config config){
		this.config = config;
	}

	public Config getConfig(){
		return config;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

}