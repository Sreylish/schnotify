package com.interview.schnotify.Login.entity;

import com.google.gson.annotations.SerializedName;


public class Profile{

	@SerializedName("name")
	private String name;

	@SerializedName("code")
	private String code;

	@SerializedName("tel")
	private String tel;

	@SerializedName("login_type")
	private String login_type;

	@SerializedName("device_key")
	private String device_key;

	@SerializedName("type")
	private String Type;

	@SerializedName("app_id")
	private String app_id;

	@SerializedName("photo")
	private String photo;

	@SerializedName("id")
	private String id;


	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setPhoto(String photo){
		this.photo = photo;
	}

	public String getPhoto(){
		return photo;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public void setLogin_type(String login_type) {
		this.login_type = login_type;
	}

	public void setDevice_key(String device_key) {
		this.device_key = device_key;
	}

	public void setType(String type) {
		Type = type;
	}

	public void setApp_id(String app_id) {
		this.app_id = app_id;
	}

	public String getTel() {
		return tel;
	}

}